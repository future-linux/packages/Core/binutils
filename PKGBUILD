# This is an example PKGBUILD file. Use this as a start to creating your own,
# and remove these comments. For more information, see 'man PKGBUILD'.
# NOTE: Please fill out the license field for your package! If it is unknown,
# then please put 'unknown'.

# Maintainer: Future Linux Team <future_linux@163.com>
pkgname=binutils
pkgver=2.43.1
pkgrel=1
pkgdesc="A set of programs to assemble and manipulate binary and object files"
arch=('x86_64')
url="https://www.gnu.org/software/binutils/"
license=('GPL-2.0-or-later' 'GPL-3.0-or-later' 'LGPL-2.0-or-later' 'LGPL-3.0-or-later' 'GFDL-1.3' 'FSFAP')
groups=('base-devel')
depends=('glibc' 'zlib' 'zstd' 'jansson' 'libelf')
backup=(etc/gprofng.rc)
options=('!distcc' '!ccache')
source=(https://sourceware.org/pub/${pkgname}/releases/${pkgname}-${pkgver}.tar.xz
    gold-warn-unsupported.patch)
sha256sums=(13f74202a3c4c51118b797a39ea4200d3f6cfbe224da6d1d95bb938480132dfd
    2d430b66f84a19c154725ec535280c493be1d34f1a90f95208dce02fecd0e4e4)

prepare() {
    cd ${pkgname}-${pkgver}

    mkdir -v build

    patch -Np1 -i ${srcdir}/gold-warn-unsupported.patch

}

build() {
    cd ${pkgname}-${pkgver}/build

    ${BUILD_CONFIGURE}                                \
        --target=${CHOST}                             \
        --sysconfdir=/etc                             \
        --with-lib-path=/usr/lib64                    \
        --enable-cet                                  \
        --enable-colored-disassembly                  \
        --enable-default-execstack=no                 \
        --enable-deterministic-archives               \
        --enable-gold                                 \
        --enable-install-libiberty                    \
        --enable-jansson                              \
        --enable-ld=default                           \
        --enable-new-dtags                            \
        --enable-pgo-build=lto                        \
        --enable-plugins                              \
        --enable-relro                                \
        --enable-shared                               \
        --enable-targets=x86_64-pep,bpf-unknown-none  \
        --enable-threads                              \
        --disable-gdb                                 \
        --disable-gdbserver                           \
        --disable-libdecnumber                        \
        --disable-readline                            \
        --disable-sim                                 \
        --disable-werror                              \
        --with-debuginfod                             \
        --with-pic                                    \
        --with-system-zlib                            \
        --with-bugurl=https://gitea.futurelinux.xyz/packages/binutils/issues

    make tooldir=/usr
}

package() {
    cd ${pkgname}-${pkgver}/build

    make DESTDIR=${pkgdir} tooldir=/usr install

    rm ${pkgdir}/usr/share/man/man1/{dlltool,windres,windmc}*

}
